@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Dashboard</div>

                    <div class="panel-body">
                        @if($rows)
                            {!! $table->render() !!}
                        @else
                            <p>No Transactions</p>
                        @endif
                        <a href="{{url('/transactions/create')}}" class="btn btn-primary pull-right">
                            Account recharge
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
