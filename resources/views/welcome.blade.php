@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Referral Links</div>

                <div class="panel-body">
                    @if($rows)
                        {!! $table->render() !!}
                    @else
                        <p>No Links available</p>
                    @endif
                    <a href="{{url('/referral-links/create')}}" class="btn btn-primary pull-right">
                        Add Link
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

