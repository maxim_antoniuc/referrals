@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">New transaction</div>
                    <div class="panel-body">
                        <form class="form-horizontal" role="form" method="POST"
                              action="{{ url('/referral-links/create') }}">
                            {{ csrf_field() }}

                            <div class="form-group{{ $errors->has('referral_link') ? ' has-error' : '' }}">
                                <label for="amount" class="col-md-4 control-label">Referral Link</label>

                                <div class="col-md-7">
                                    <div class="">

                                        <input id="referral_link" type="referral_link" class="form-control" name="referral_link"
                                               value="{{ old('referral_link') }}">
                                    </div>


                                    @if ($errors->has('referral_link'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('referral_link') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        <i class="fa fa-btn fa-sign-in"></i> Save
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection