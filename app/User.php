<?php

namespace App;

use App\Http\Models\Referrals;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * @param $user
     * @return mixed
     */
    public static function parent($user_id){

        $user = new Referrals();
        return $user->where('id_users',$user_id)->first();

    }
}
