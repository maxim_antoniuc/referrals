<?php

namespace App\Http\Models;
use Illuminate\Database\Eloquent\Model;
use Gbrock\Table\Traits\Sortable;

class ReferralLinks extends Model {

	protected $table = 'referral_links';
	public $timestamps = true;
	use Sortable;

	protected $hidden = array('id_users');


}