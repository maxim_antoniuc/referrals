<?php

namespace App\Http\Models;
use Illuminate\Database\Eloquent\Model;

class Referrals extends Model {

	protected $table = 'referrals';
	public $timestamps = true;


	/**
	 * @param $parent
	 * @param $referral
	 */
	public static function saveReferral($parent,$referral){

		$referral = new Referrals();
		$referral->parent_id=$parent;
		$referral->id_referral=$referral;
		$referral->save();

	}

}