<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Gbrock\Table\Traits\Sortable;

class Transactions extends Model
{
    use Sortable;

    protected $table = 'transactions';
    public $timestamps = true;
    protected $hidden = array('id_users', 'id_referrals');
    protected $sortable = ['id', 'amount', 'created_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function referral(){

        return $this->hasOne('App\User','id','id_referrals');
    }

    /**
     * @param $user
     * @param $amount
     */
    public static function saveTransaction($user, $amount){
        
        $transaction = new Transactions();
        $transaction->amount = $amount;
        $transaction->id_users = $user;
        $transaction->save();
    }

    /**
     * @param $referral
     * @param $fee
     * @return Transactions
     */
    public static function saveReferralFee($user,$referral, $fee)
    {
        $transaction = new Transactions();
        $transaction->amount = $fee;
        $transaction->id_users = $user;
        $transaction->id_referrals = $referral;
        $transaction->save();

        return $transaction;
    }

}