<?php

namespace App\Http\Controllers;


use App\Http\Models\ReferralLinks;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class ReferralLinksController extends Controller
{

    protected $path;

    public function __construct()
    {

        $this->path = url('referral-links/token/');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {

    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function getToken(Request $request)
    {

        $link = ReferralLinks::where('referral_link', $request->fullUrl())->first();

        if ($link) {
            Session::put('parent', $link->id_users);
        }

        return redirect()->guest('register');
    }

    /**
     * @return mixed
     */
    public function getCreate()
    {
        $reff = new ReferralLinks();
        $reff->id_users = Auth::user()->id;
        $reff->referral_link = $this->path.'/'. sha1(time());
        $reff->save();

        return redirect('/');
    }
}

?>