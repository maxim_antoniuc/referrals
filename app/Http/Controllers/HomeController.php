<?php

namespace App\Http\Controllers;

use App\Http\Models\ReferralLinks;
use App\Http\Requests;
use Gbrock\Table\Facades\Table;
use Illuminate\Http\Request;
use App\Http\Models\Transactions;
use App\Http\Models\Referrals;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @return mixed
     */
    public function index()
    {
        $links = new ReferralLinks();
        $rows = $links->where('id_users', Auth::user()->id)->get(); // Get all transactions from the database

        $table = Table::create($rows); // Generate a Table based on these "rows"

        return view('welcome',['table' => $table, 'rows' => $links->count()]);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function home()
    {
        $transactions = new Transactions();
        $rows = $transactions->where('id_users', Auth::user()->id)->get(); // Get all transactions from the database

        $table = Table::create($rows); // Generate a Table based on these "rows"

        // We pass in the field, label, and a callback accepting the model data of the row it's currently rendering
        $table->addColumn('id_referrals', 'Referral', function ($transactions) {

            $referral_name=null;
            $referral = $transactions->referral()->first();

            if($referral){
                $referral_name=$referral->name;
            }

            return $referral_name ? $referral_name : 'N/A';
        });

        return view('home', ['table' => $table, 'rows' => $rows->count()]);
    }
}
