<?php

namespace App\Http\Controllers;

use App\Http\Models\ReferralTransactions;
use App\Http\Models\Transactions;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;
use Validator;

class TransactionsController extends Controller
{
    public $fee = 0.1;

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view('transactions.list');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function getCreate()
    {
        return view('transactions.create');
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function postCreate(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'amount' => 'required|numeric'
        ]);

        if ($validator->fails()) {
            return redirect('/transactions/create')
                ->withErrors($validator)
                ->withInput();
        } else {
            $amount_paid = $request->amount;

            if ($user = User::parent(Auth::user()->id)) {
                $amount = $amount_paid * (1 - $this->fee);

                //sent 10% from amount paid to parent referral
                Transactions::saveReferralFee(
                    $user->parent_id,
                    Auth::user()->id,
                    $amount_paid - $amount
                );
            } else {
                $amount = $amount_paid;
            }

            //Save transaction
            Transactions::saveTransaction(
                Auth::user()->id,
                $amount
            );

            return redirect('/home');
        }
    }
}

?>