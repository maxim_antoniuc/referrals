<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::controller('transactions', 'TransactionsController');
Route::controller('referral-links', 'ReferralLinksController');

Route::auth();

Route::get('/home', 'HomeController@home');
Route::get('/', 'HomeController@index');
