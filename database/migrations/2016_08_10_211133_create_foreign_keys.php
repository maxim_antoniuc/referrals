<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateForeignKeys extends Migration {

	public function up()
	{
		Schema::table('referrals', function(Blueprint $table) {
			$table->foreign('parent_id')->references('id')->on('users')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
		Schema::table('referrals', function(Blueprint $table) {
			$table->foreign('id_users')->references('id')->on('users')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('transactions', function(Blueprint $table) {
			$table->foreign('id_users')->references('id')->on('users')
						->onDelete('restrict')
						->onUpdate('restrict');
		});

		Schema::table('referral_links', function(Blueprint $table) {
			$table->foreign('id_users')->references('id')->on('users')
						->onDelete('restrict')
						->onUpdate('restrict');
		});

	}

	public function down()
	{
		Schema::table('referrals', function(Blueprint $table) {
			$table->dropForeign('referrals_parent_id_foreign');
		});
		Schema::table('referrals', function(Blueprint $table) {
			$table->dropForeign('referrals_id_users_foreign');
		});
		Schema::table('transactions', function(Blueprint $table) {
			$table->dropForeign('transactions_id_users_foreign');
		});

		Schema::table('referral_links', function(Blueprint $table) {
			$table->dropForeign('referral_links_id_users_foreign');
		});


	}
}