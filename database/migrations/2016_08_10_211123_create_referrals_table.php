<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateReferralsTable extends Migration {

	public function up()
	{
		Schema::create('referrals', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('parent_id')->unsigned();
			$table->integer('id_users')->unsigned();
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('referrals');
	}
}