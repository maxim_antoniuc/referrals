<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTransactionsTable extends Migration {

	public function up()
	{
		Schema::create('transactions', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('id_users')->unsigned();
			$table->float('amount', 10,2);
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('transactions');
	}
}