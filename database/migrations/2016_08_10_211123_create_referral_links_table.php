<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateReferralLinksTable extends Migration {

	public function up()
	{
		Schema::create('referral_links', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('id_users')->unsigned();
			$table->string('referral_link', 255);
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('referral_links');
	}
}